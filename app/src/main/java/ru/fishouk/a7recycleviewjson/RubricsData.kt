package ru.fishouk.a7recycleviewjson

class RubricsData(mRubricId: String, mRubricName: String) {
    var mRubricId: String
        internal set
    var mRubricName: String
        internal set


    init {
        this.mRubricId = mRubricId
        this.mRubricName = mRubricName
    }
    fun getId() : String {return this.mRubricId}
    fun getName() : String {return this.mRubricName}
}