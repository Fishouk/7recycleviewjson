package ru.fishouk.a7recycleviewjson

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.ListView
import android.widget.SimpleAdapter
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URL
import java.util.*

class listRubrics : AppCompatActivity() {
    companion object {
        private var rubrics = ArrayList<RubricsData>()
        private var recyclerView: RecyclerView? = null
        private var adapter: RecyclerView.Adapter<RubricsAdapter.MyViewHolder>? = null

       // private var mDataFinishArr = ArrayList<HashMap<String, String>>()
        private val LOG_TAG = "my_log"
    }

    private var layoutManager: RecyclerView.LayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_rubrics)
        recyclerView = findViewById(R.id.recyclerViewRubrics) as RecyclerView
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.setLayoutManager(layoutManager)

        adapter = RubricsAdapter(this@listRubrics, rubrics) //Инициализируем наш адаптер
        // Устанавливаем адаптер
        ParseTask().execute() //Заполняем массив

    }

    private class ParseTask : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void): String{

            val mResultJson: String = URL("http://ad.api.zigzag-mobile.com/public/getRubrics").readText()


            return mResultJson;

        }

        override fun onPostExecute(mStrJson: String) {
            super.onPostExecute(mStrJson)
            val mDataJasonObj: JSONObject

            try {
                mDataJasonObj = JSONObject(mStrJson)
                val mList: JSONArray = mDataJasonObj.getJSONObject("data").getJSONArray("list")
                for (i in 0..mList.length() - 1) {
                    val mListItem = mList.getJSONObject(i)

                    rubrics.add(RubricsData(mListItem.getString("id"), mListItem.getString("name")))
                    Log.d(LOG_TAG, rubrics.toString())
                }
                recyclerView!!.adapter = adapter
            }
            catch (e: JSONException) {
                e.printStackTrace()
                Log.d(LOG_TAG, e.toString())
            }
        }
    }

}