package ru.fishouk.a7recycleviewjson

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

class RubricsAdapter(var mContext: Context, private val rubricsDataSet: ArrayList<RubricsData>)
: RecyclerView.Adapter<RubricsAdapter.MyViewHolder>() {
    override fun getItemCount(): Int {
        return rubricsDataSet.size
    }

    private val lastPosition = -1

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textId: TextView
        internal var textName: TextView

        init {
            this.textId = itemView.findViewById(R.id.rubricsId) as TextView
            this.textName = itemView.findViewById(R.id.rubricsName) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                           viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rubric_card, parent, false)
        val myViewHolder = MyViewHolder(view)

        return myViewHolder
    }


    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {

        val textViewId = holder.textId
        val textViewName = holder.textName

        textViewId.setText(rubricsDataSet.get(listPosition).getId())
        textViewName.setText(rubricsDataSet.get(listPosition).getName())
    }
}

