package ru.fishouk.a7recycleviewjson

import android.content.Intent
import android.net.sip.SipAudioCall
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mButtonGetData: Button? = findViewById(R.id.buttonGetData) as Button
        val mActivityListRubrics = Intent(this, listRubrics::class.java)
        mButtonGetData!!.setOnClickListener() {
            startActivity(mActivityListRubrics)
        }
    }
}